package br.com.hrtech.fontes;

public class Cliente
{

	private String	Nome;
	private int		idCliente;

	public Cliente(String Nome, int idCliente)
	{
		super();
		this.Nome = Nome;
		this.idCliente = 1;
	}

	public Cliente()
	{
	}

	public String getNome()
	{
		return Nome;
	}

	public void setNome(String nome)
	{
		Nome = nome;
	}

	public int getIdCliente()
	{
		return idCliente;
	}

	public void setIdCliente(int idCliente)
	{
		this.idCliente = idCliente;
	}

}
