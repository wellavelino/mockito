package br.com.hrtech.fontes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

public class OcorrenciaDAO
{
	private static final String	JDBC_DRIVER	= "net.sourceforge.jtds.jdbc.Driver";
	private static final String	JDBC_URL	= "jdbc:jtds:sqlserver://192.168.0.200:1433/TESTEDBUNIT";
	private static final String	USUARIO		= "sa";
	private static final String	SENHA		= "HRP@ssword";
	private static final String	NOME		= "Wellington";
	private static final int	ID			= 1;

	public void cliente(Cliente cliente) throws ClassNotFoundException, SQLException, DatabaseUnitException, FileNotFoundException, IOException
	{
		Class<?> driverClass = Class.forName(JDBC_DRIVER);

		Connection jdbcConnection = DriverManager.getConnection(JDBC_URL, USUARIO, SENHA);
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
		QueryDataSet parcialDataSet = new QueryDataSet(connection);
		parcialDataSet.addTable("clientes", "SELECT * FROM clientes ");
		FlatXmlDataSet.write(parcialDataSet, new FileOutputStream("testedd3.xml"));

		try (Connection con = connection.getConnection(); PreparedStatement pstmt = con.prepareStatement("insert into clientes (nome_cliente,id_cliente) values(?,?)");)
		{
			pstmt.setString(1, NOME);
			pstmt.setInt(2, ID);
			pstmt.executeUpdate();

			
		}

	}
}
