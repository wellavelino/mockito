package br.com.hrtech.NotaFiscal;

public class NotaFiscal
{
	private String	nome;
	private double	valorTotal;

	public NotaFiscal(String nome, double valorTotal)
	{
		super();
		this.nome = nome;
		this.valorTotal = valorTotal;

	}

	public String getNome()
	{
		return nome;
	}

	public double getValorTotal()
	{
		return valorTotal;
	}

}
