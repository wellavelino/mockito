package br.com.hrtech.NotaFiscal;

public class CalculadorValorNota
{
	private CalculadorDAO	calculadorDAO;

	public CalculadorValorNota(CalculadorDAO calculadorDAO)
	{
		super();
		this.calculadorDAO = calculadorDAO;
	}

	public NotaFiscal calculadorTotalNotaFiscal(Pedido pedido)
	{

		NotaFiscal notaFiscal = new NotaFiscal(pedido.getNome(), pedido.getValorPedido()+50.00);

		calculadorDAO.calculadorNotaFiscal(notaFiscal);

		return notaFiscal;

	}

}
