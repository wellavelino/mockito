package br.com.hrtech.NotaFiscal;


public class Pedido
{
	String	nome;
	double	valorPedido;

	public Pedido(String nome, double valorPedido)
	{
		super();
		this.nome = nome;
		this.valorPedido = valorPedido;
	}

	public String getNome()
	{
		return nome;
	}

	public double getValorPedido()
	{
		return valorPedido;
	}

}
