package br.com.hrtech.Beans;

public class Compra
{
	private String	nome;
	private double	valorgasto;

	public String getNome()
	{
		return nome;
	}

	public double getValorgasto()
	{
		return valorgasto;
	}

	public Compra(String nome, double valorgasto)
	{
		super();
		this.nome = nome;
		this.valorgasto = valorgasto;
	}

}