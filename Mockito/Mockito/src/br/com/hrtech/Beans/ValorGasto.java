package br.com.hrtech.Beans;

public class ValorGasto
{

	private String	nome;
	private double	total;

	public ValorGasto(String nome, double total)
	{
		super();
		this.nome = nome;
		this.total = total;
	}

	public String getNome()
	{
		return nome;
	}

	public double getTotal()
	{
		return total;
	}

}
